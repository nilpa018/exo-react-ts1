# Exercice React + TypeScript : Création d'une application de traduction v1

## Objectif

Le première version de cet exercice propose une version basique de l'interface qui sera ensuite amélioré afin de fabriquer un outil simple de traduction en ligne avec React et TypeScript.

## Prérequis

Assurez-vous d'avoir Node.js installé sur votre machine.

## Étapes

### Étape 1: Initialisation du projet React

1. Clonez le repository depuis GitLab :

   ```bash
   git clone https://gitlab.com/nilpa018/exo-react-ts1.git
   ```

2. Accédez au répertoire du projet :

   ```bash
   cd exo-react-ts1
   ```

3. Lancez l'installation des modules et construction du dossier public :

   ```bash
   npm install
   ```

4. Lancez l'application pour vous assurer que tout fonctionne correctement :
   ```bash
   npm run dev
   ```

### Étape 2: Contenu du projet

Le projet est constitué comme ceci :

Dossier src

- App.tsx: Le composant principale
- app.css: La feuille de style du composant App.tsx

Sous dossier assets: il contient l'image TypeScript dans le composant App.

Sous dossier components

- Translate.tsx: Le composant contient les instructions ainsi qu'un input et un bouton.
- translate.css: La feuille de style du composant Translate.tsx

Sous dossier utils

- Constant.ts: Le composant contient les constantes de l'application
- Functions.ts: Le composant contient une fonction qui affiche une alerte avec le texte et la langue.

### Étape 3: Objectif de l'exercice

Vous devez typer dans ce projet les constantes et fonctions en créant un fichier Types.ts dans le sous dossier utils afin d'y ajouter tous les types et leurs noms que vous trouverez dans l'utilisation afin de protéger au mieux l'intégrité de l'application.

Vous pouvez si vous le souhaitez utiliser l'Union Types afin de rajouter des valeurs de votre choix sans modifier l'interface utilisateur.

**Dans la version 2 et 3 de l'exercice, nous feront appel à une Api de traduction.**

### Étape 4: Conditions de réussite

Toutes les constantes et fonctions sont typées et reliées avec le fichier Types.ts
Aucun type "any" n'est permis.   
Si plusieurs types sont possibles, il faut les regrouper.   
Le linteur doit être actif doit passer sans erreurs ni avertissements.  
Lancez le linteur avant de retourner l'exercice.

Assurez-vous d'inclure le code source complet pour la vérification.

### Étape 5: Vérification

Le candidat devra fournir l'ensemble des fichiers à l'exception des node_modules après avoir complété la tâche.

### Aperçu graphique

Voici une représentation de ce qui est attendu graphiquement, vous avez la possibilité de modifier un peu celui-ci.

![Representation graphique de l'exercice](/images/exo-react-ts1.png)

### Remarques

- Afin de répondre aux meilleurs pratiques, déclarez vos variables de préférences avec const et let.
- Vous avez le droit d'ajouter des fonctionnalités au projet (loader, tests, TypeScript etc...)
- Utilisez les composants de manière modulaire et réutilisable.
- Commentez votre code pour expliquer votre démarche si c'est pertinent.

### Ressource

Documentation React : https://reactjs.org/docs/getting-started.html  
Documentation TypeScript : https://www.typescriptlang.org/

### Soumission

Lorsque vous avez terminé, assurez-vous de fournir le code source du projet pour évaluation soit en l'envoyant au format ZIP sans y inclure les node_modules par [E-mail](mailto:nilpa018@yahoo.fr) soit en l'hébergeant sur un Drive afin de pouvoir télécharger celui-ci.
