
import { useState } from 'react';
import { TRANSLATE_BUTTON, TRANSLATE_INPUT, INSTRUCTIONS } from '../utils/Constants';
import './translate.css';
import { submitTranslation } from '../utils/Functions';

const Translate = () => {
    const [value, setValue] = useState(undefined);
    const [language, setLanguage] = useState('EN');

    const handleChange = (e) => {
        setValue(e.target.value);
    }
    const handleChangeLang = (e) => {
        setLanguage(e.target.value);
    }
    const handleSubmit = () => {
        submitTranslation(value, language);
    }

    return (
        <>
            <p>{INSTRUCTIONS.translate}</p>
            <div className="translate">
                <input type="text" placeholder={TRANSLATE_INPUT} value={value} onChange={handleChange} />
                <input type="text" placeholder="Lang" size={2} maxLength={2} value={language} onChange={handleChangeLang} />
                <button type="button" onClick={handleSubmit} disabled={!value || !language}>{TRANSLATE_BUTTON}</button>
            </div >
        </>
    )

}

export default Translate