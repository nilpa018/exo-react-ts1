export const EXERCICE_TITLE = "Type Moi !";
export const EXERCICE_VERSION = 1;
export const TRANSLATE_BUTTON = "Traduire";
export const TRANSLATE_INPUT = "Texte";
export const TRANSLATION_LANG = "EN";

export const INSTRUCTIONS = {
  exercice: `Le but de cet exercice est de typer tout ce que vous pouvez avec TypeScript, 
    nous construirons en 3 étapes un traducteur de texte en React et TypeScript`,
  translate: "Veuillez entrer un texte à traduire et cliquer sur Translate",
};
